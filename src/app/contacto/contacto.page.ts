import { Component, OnInit } from '@angular/core';
import { ToastController, Platform } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.page.html',
  styleUrls: ['./contacto.page.scss'],
})
export class ContactoPage implements OnInit {
  public counter = 0;
  constructor(
    public toastController: ToastController, private router: Router, private platform: Platform
  ) {
    this.platform.backButton.subscribe(async () => {
      console.log("entre aqui");
      if (this.router.isActive('/contacto', true) && this.router.url === '/contacto') {
    if (this.counter == 0) {
          this.counter++;
          this.presentToast();
          setTimeout(() => { this.counter = 0 }, 3000)
        } else{
          navigator['app'].exitApp();
        }
      }
    });
   }
   
  async  presentToast() {
    let toast = await this.toastController.create({
      message: "Presione dos veces para salir",
      duration: 3000,
    });
    toast.present();
  }

  ngOnInit() {
  }

}
