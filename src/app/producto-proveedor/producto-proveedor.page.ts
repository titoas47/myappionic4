import { Component, OnInit } from '@angular/core';
import { Storage} from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';
import { Md5 } from 'ts-md5';
import { NavController,AlertController } from '@ionic/angular';
import { NavigationExtras } from "@angular/router";

@Component({
  selector: 'app-producto-proveedor',
  templateUrl: './producto-proveedor.page.html',
  styleUrls: ['./producto-proveedor.page.scss'],
})
export class ProductoProveedorPage implements OnInit {
  ngOnInit() {
   
  }
  public producto: any;
  public producto_proveedores: any;
  public proveedores: any;

  constructor(
    private navCtrl: NavController,
    public storage: Storage,
    public route: ActivatedRoute,
    public  alertCtrl: AlertController
  ) {
    this.route.queryParams.subscribe(params => {
      if (params["producto"]) {
        this.producto = JSON.parse(params["producto"]);
      } else {
        this.producto = {};
      }
    });
    //recuperar los precios de los proveedores del producto
    let producto_a_buscar=this.producto;
    this.producto_proveedores=[];
    this.storage.get("producto_proveedores").then((producto_proveedores)=>{
    this.producto_proveedores = producto_proveedores.filter(function(e,index){return e.id_producto == producto_a_buscar.id;});
    });
    //recuperar la tabla proveedorees
    this.proveedores=[];
    this.storage.get("proveedores").then((proveedores)=>{
      this.proveedores=proveedores;
    });
  }

  ionViewDiEnter() {

  }
  crearPrecio(){
    let NavigationExtras:NavigationExtras={
      queryParams:{
        producto:JSON.stringify(this.producto)
      }
    }
    this.navCtrl.navigateForward(['edicion-producto-proveedor'], NavigationExtras);
  }
  mostrarNombreProveedor(id_proveedor){
      let proveedorEncontrado= this.proveedores.filter(function(e,index){return e.id== id_proveedor;})[0];
      if (proveedorEncontrado) {
        return proveedorEncontrado.nombre;

      } else {
        return " ";
      }
  }
  // eliminarProductoProveeedor(producto_proveedor){
  //   // let productoEncontrado=this.productos.filter(function(e,index){return e.id == producto.id;})[0];
  //   this.producto_proveedores.splice((this.producto_proveedores.indexOf(producto_proveedor)),1);
  //   this.storage.set("producto_proveedores",this.producto_proveedores);
  // }
  async presentAlertConfirm(producto_proveedor) {
    const alert = await this.alertCtrl.create({
      header: 'Alerta!',
      message: '<strong>Desea eliminar</strong>!!!',
      buttons: [
        {
          text: 'Cancel',
          cssClass: 'secondary',
        }, {
          text: 'ok',
          handler: () => {
            this.producto_proveedores.splice((this.producto_proveedores.indexOf(producto_proveedor)),1);
            this.storage.set("producto_proveedores",this.producto_proveedores);
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
}
