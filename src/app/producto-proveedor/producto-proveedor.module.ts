import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProductoProveedorPage } from './producto-proveedor.page';

const routes: Routes = [
  {
    path: '',
    component: ProductoProveedorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProductoProveedorPage]
})
export class ProductoProveedorPageModule {}
