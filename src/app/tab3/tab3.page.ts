import { Component } from '@angular/core';
import { Storage} from '@ionic/storage';
import { Router } from '@angular/router';
import { NavigationExtras } from "@angular/router";
import { NavController,AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  public proveedores: any;
  constructor(
    private router: Router,
    private navCtrl: NavController,
    private storage: Storage,
    private alertCtrl: AlertController

  ) {
    this.storage.get("proveedores").then((proveedorDeMeroria) => {
      this.proveedores = proveedorDeMeroria;
    });
  }
  pushList() {
    this.router.navigateByUrl(`/edicion-proveedor`);
  }
  ionViewDidenter() {
    this.storage.get("proveedores").then(proveedorDeMeroria => {
      this.proveedores = proveedorDeMeroria;
    });
  }
  modificarProveedor(proveedor) {
    let NavigationExtras:NavigationExtras={
      queryParams:{
        proveedor:JSON.stringify(proveedor)
      }
    }
    this.navCtrl.navigateForward(['edicion-proveedor'], NavigationExtras);
  }
  eliminarProducto(proveedor){
    // let productoEncontrado=this.productos.filter(function(e,index){return e.id == producto.id;})[0];
    this.proveedores.splice((this.proveedores.indexOf(proveedor)),1);
    this.storage.set("proveedores",this.proveedores);
  }
  async presentAlertConfirm(proveedor) {
    const alert = await this.alertCtrl.create({
      header: 'Alerta!',
      message: ' <strong>Desea eliminar</strong>!!!',
      buttons: [
        {
          text: 'Cancel',
          cssClass: 'secondary'
        }, {
          text: 'ok',
          handler: () => {
            this.proveedores.splice((this.proveedores.indexOf(proveedor)),1);
            this.storage.set("proveedores",this.proveedores);
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
}
