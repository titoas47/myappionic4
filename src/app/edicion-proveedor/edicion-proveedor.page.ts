import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';
import { Md5 } from 'ts-md5';

@Component({
  selector: 'app-edicion-proveedor',
  templateUrl: './edicion-proveedor.page.html',
  styleUrls: ['./edicion-proveedor.page.scss'],
})
export class EdicionProveedorPage implements OnInit {
  public proveedor: any;
  constructor(
    private navCtrl: NavController,
    private storage: Storage,
    public route: ActivatedRoute,
    private alertCtrl: AlertController

  ) {
    this.route.queryParams.subscribe(params => {
      if (params["proveedor"]) {
        this.proveedor = JSON.parse(params["proveedor"]);
      } else {
        this.proveedor = {};
      }
    });
  }

  ngOnInit() {

  }
  cancelar() {
    this.navCtrl.navigateForward(``);
  }
  guardar() {
    this.storage.get("proveedores").then(async (proveedores) => {
      if (this.proveedor.id) {
        let proveedorBuscado = this.proveedor;
        let proveedorEncontrado = proveedores.filter(function (e, index) { return e.id == proveedorBuscado.id; })[0];
        proveedores[proveedores.indexOf(proveedorEncontrado)] = proveedorBuscado;
        this.storage.set("proveedores", proveedores);
      } else {
        this.proveedor.id = Md5.hashStr(new Date().getTime().toString());
        if (proveedores) {
          proveedores.push(this.proveedor);
          this.storage.set("proveedores", proveedores);
        } else {
          let arrayProveedores = [];
          arrayProveedores.push(this.proveedor);
          this.storage.set("proveedores", arrayProveedores);
        }
      }
      this.navCtrl.navigateBack("/tabs/tab3");
      const alert = await this.alertCtrl.create({
        header: 'Informacion',
        message: 'Proveedor registrado exitosamente',
        buttons: [
          {
            text: 'ok',
          }
        ]
      });

      await alert.present();
    });
  }
}
