import { Component, OnInit } from "@angular/core";
import { NavController,AlertController } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { Md5 } from "ts-md5/dist/md5";
import { ActivatedRoute } from "@angular/router";
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: "app-edicion-producto",
  templateUrl: "./edicion-producto.page.html",
  styleUrls: ["./edicion-producto.page.scss"]
})
export class EdicionProductoPage implements OnInit {
  public producto: any;
  public imgPrueba : any;
  base64Image;
  constructor(
    private navCtrl: NavController,
    private storage: Storage,
    public route: ActivatedRoute,
    private alertCtrl:AlertController,
    private camera: Camera
   
  ) {
    this.route.queryParams.subscribe(params => {
      if (params["producto"]) {
        this.producto = JSON.parse(params["producto"]);
      } else {
        this.producto = {};
      }
    });
  }

  ngOnInit() {}
  cancelar() {
    this.navCtrl.navigateForward(``);
  }
  guardar() {
    this.storage.get("productos").then(async(productos)=>{
      if (this.producto.id) {
        let productoBuscado=this.producto;
        let productoEncontrado= productos.filter(function(e,index){return e.id ==productoBuscado.id;})[0];
        productos[productos.indexOf(productoEncontrado)]=productoBuscado;
        this.storage.set("productos", productos);
      } else {
        this.producto.id = Md5.hashStr(new Date().getTime().toString());
          if (productos) {
            productos.push(this.producto);
            this.storage.set("productos", productos);
          } else {
            const arrayProductos = [];
            arrayProductos.push(this.producto);
            this.storage.set("productos", arrayProductos);
          }
          
      }
      this.navCtrl.navigateBack("");
      const alert = await this.alertCtrl.create({
        header: 'Informacion',
        message: 'Producto registrado satisfactoriamente',
        buttons: [
           {
          text: 'ok',
          }
        ]
      });
  
      await alert.present();
    
    });
  }
  capturarImagen(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    
    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
    //  let base64Image = 'data:image/jpeg;base64,' + imageData;
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.producto.imagen=base64Image;
    }, (err) => {
     // Handle error
    });
  }
  albumImagen(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:false
    }
    
    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
    //  let base64Image = 'data:image/jpeg;base64,' + imageData;
    let base64Image = 'data:image/jpeg;base64,' + imageData;
    this.producto.imagen=base64Image;
    }, (err) => {
     // Handle error
    });
  }
}
