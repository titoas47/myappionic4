import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { NavigationExtras } from "@angular/router";
import { InAppBrowser,InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  public productos: any;
  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
};
  constructor(
    private router: Router,
    private navCtrl: NavController,
    private storage: Storage,
    private alertCtrl: AlertController,
    private iab: InAppBrowser,
  ) {
    this.storage.get("productos").then(productosDeMeroria => {
      this.productos = productosDeMeroria;
    });
  }
  
  pushListProducto() {
    this.router.navigateByUrl(`/edicion-producto`);
  }
  verprecios(producto) {
    let NavigationExtras: NavigationExtras = {
      queryParams: {
        producto: JSON.stringify(producto)
      }
    }
    this.navCtrl.navigateForward(['producto-proveedor'], NavigationExtras);
  }
  ionViewDidenter() {
    this.storage.get("productos").then(productosDeMeroria => {
      this.productos = productosDeMeroria;
            //vibration
            // this.vibration.vibrate([2000,1000,2000]);
    });
  }
  modificarProducto(producto) {
    let NavigationExtras: NavigationExtras = {
      queryParams: {
        producto: JSON.stringify(producto)
      }
    }
    this.navCtrl.navigateForward(['edicion-producto'], NavigationExtras);
  }
  // eliminarProducto(producto){
  //   this.productos.splice((this.productos.indexOf(producto)),1);
  //   this.storage.set("productos",this.productos);
  // }
  async presentAlertConfirm(producto) {
    const alert = await this.alertCtrl.create({
      header: 'Alerta!',
      message: '<strong>Desea eliminar</strong>!!!',
      buttons: [
        {
          text: 'Cancel',
          cssClass: 'secondary',
        }, {
          text: 'ok',
          handler: () => {
            this.productos.splice((this.productos.indexOf(producto)),1);
            this.storage.set("productos", this.productos);
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
  apiWhatsapp(producto){
   
    const browser = this.iab.create('https://api.whatsapp.com/send?phone=59169529656'+producto+'&text=Hola te tu mes esta por vecer?');
  
  }

}
